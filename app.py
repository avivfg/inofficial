import sys
from time import strftime
from logging import DEBUG
from os import path, makedirs
from preprocess import preprocess
from collections import defaultdict
from exports import inofficialities_exports
from analyses import inofficialities_analyses
from profanity.profanity_lookup import lookup as profanity_lookup 
from punctuation.punctuation_lookup import find_ino_punct
from emojicon.emoji_detection import lookup as emoji_lookup
from utils import create_logger, lower_case_index_keys, eliminate_words
from punctuation.punctuation_utils import punct_cleaning as punct_cleaner
from ALL_CAPS_SHOUTING.all_caps_shouting import lookup as all_caps_shouting_lookup
# from tkinter import Tk                         | for the 
# from tkinter.filedialog import askdirectory    | input.


"""
"Inofficial" - detection of unofficial language and unofficial behavior in big email datasets.
"""

NAME_SPACE = "EDT"
APP_NAME = "inofficial" # NICK_NAME = "ino"
SPACED_NAME = "Unofficial Behaviour Detector" # ACRONYM = "UBD"

################################################################
# Logger initiation:
logger = create_logger(APP_NAME + '-' + NAME_SPACE, DEBUG)
logger.debug(sys.path)
################################################################


"""
This file contains the main functions of the code.
From getting the input dataset, to setting the output folders
and call the particular functions - everything starts here.
--> Implementing a new task should be independent and locate
    in a different place, but would be called through here.
"""

###################
# Input and Output:
###################

def get_input():
    # Get the input dataset with a tkFileDialog (popping window): (activate the imports)
    # root = Tk()
    # root.directory = askdirectory()
    # print(root.directory)
    # return root.directory

    # Get the input dataset by default:
    if len(sys.argv) == 1:
        dataset = "C:/Users/avivf/Desktop/inputs/bodies"
    else:
        # Get the input dataset from command line:
        dataset = ' '.join(sys.argv[1:])
    print('\nRunning ', dataset, ':\n')
    return dataset


def set_output(dir_path, task_name=None):
    # creating the output folder with the exact running start date
    if not task_name:
        output_folder_name = "outputs"
        date = strftime("%Y-%m-%d_%H-%M")
        dataset_name = dir_path.split('/')[-2]
        output_dir = path.join("outputs", date + ' - ' + dataset_name)
        return output_dir

    output_dir = path.join(dir_path, task_name)
    excel_output_dir = path.join(output_dir, "excel output files")
    most_ranked_dir = path.join(output_dir, "most ranked bodies")
    if not path.isdir(output_dir):
                makedirs(output_dir)
                makedirs(excel_output_dir)
                makedirs(most_ranked_dir)
                # creating the excel files folder inside that output folder
    return output_dir


######################
# Directing the calls:
######################
def call_emoji_lookup(w_index_pckl_path):
    return emoji_lookup(w_index_pckl_path)

def call_AC_shouting_lookup(w_index_pckl_path):
    return all_caps_shouting_lookup(w_index_pckl_path)

def call_profanity_lookup(w_index_pckl_path, docs_pckl_path):
    return profanity_lookup(w_index_pckl_path, docs_pckl_path)
#################################################################

def more_tasks_to_do(done_tasks, tasks):
    return any(boo and task not in done_tasks for task, boo in tasks.items())

def get_user_requests(get_all_inos=False):
    """
    This method is where the decision of the active tasks is being made.
    Any task that is being implemented and wanted to be in use, must appear here.  
    """
    user_tasks = defaultdict(bool)
    if get_all_inos:
        user_tasks['Emoji'] = True
        user_tasks['Punct'] = True
        user_tasks['ALL_CAPS'] = True
        user_tasks['Profanity'] = True
        return user_tasks
    # else: ask for the tasks that the client/user wants to be done.
    user_tasks['Profanity'] = True
    return user_tasks

def main():
    """
    Creating the words index, cleaning it and calling to the lookups.
    Everything is being done in the correct order as the pipeline guides.
    """

    logger.info("Starting...")

    input_dir = get_input()
    output_dir = set_output(input_dir)




    # Collecting the words_index and sentences, and get their pickles.
    w_i_pkl_path, docs_pkl_path = preprocess(input_dir)
    # words_index's values are lists of dictionaries with fields:
    # f_name ,f_idx ,sent_idx, idx_in_sent, start, end ,line_num, original_token, left_w, right_w


    # w_i_pkl_path = 'words.pkl'
    # docs_pkl_path = 'docs.pkl'


    # asking the user for tasks.
    user_tasks = get_user_requests(True)
    # user_tasks is a dict of the tasks names and a boolean that represent the users choise.

    output_dirs = dict()
    for task in user_tasks:
        output_dirs[task] = set_output(output_dir, task)
    detected_inos_pkl_paths = dict()


    #############################################################################################################
    # call the requested detection tasks:
    #############################################################################################################


    ##############################
    # Raw text index detections:::
    ##############################
    print("emoji")
    if user_tasks['Emoji']:
        # user_tasks['In_Punct_Emojis'] = True
        detected_inos_pkl_paths['Emoji'] = \
            call_emoji_lookup(w_i_pkl_path)

    print("after emoji")

    ########### (Index is getting cleaned from puncts) ###########
    if more_tasks_to_do(detected_inos_pkl_paths ,user_tasks):
        all_puncts_path = punct_cleaner(w_i_pkl_path)
    # words_amount['after cleaning puncts'] = len(words_index)
    print("after cleaning")
    

    ######################
    # Puncts detections:::
    ######################

    if user_tasks['Punct']:
        detected_inos_pkl_paths['Punct'] = \
            find_ino_punct(all_puncts_path, docs_pkl_path)
    print("after punct")

    # if user_tasks['In_Punct_Emojis']:
    #     # check the "stand_alone"s. the rest (besides prefix emoticons) are covered by the first emoji detection.
    #     detected_inos_pkl_paths['In_Punct_Emojis'] = \
    #         detect_emojis_in_puncts(all_puncts_path, docs_pkl_path)

    ####################################
    # After cleaning index detections:::
    ####################################

    if user_tasks['ALL_CAPS']:
    # SHOULD NOT ELIMINATE WORDS BEFORE CHECKING ALL-CAPS-SHOUTING.
        detected_inos_pkl_paths['ALL_CAPS'] = \
            call_AC_shouting_lookup(w_i_pkl_path)

    ############# (Index is going lowercase) #############
    if more_tasks_to_do(detected_inos_pkl_paths ,user_tasks):
        lower_case_index_keys(w_i_pkl_path)
    # words_amount['after going lowercase'] = len(words_index)

    eliminate_words(w_i_pkl_path)
    # words_amount['after last elimination:'] = len(words_index)

    if user_tasks['Profanity']:
        detected_inos_pkl_paths['Profanity'] = \
            call_profanity_lookup(w_i_pkl_path, docs_pkl_path)



    print('######################################')
    #############################################################################################################
    # Analyses:
    #############################################################################################################


    analyses_results_pkl_path = \
        inofficialities_analyses(detected_inos_pkl_paths, docs_pkl_path)

    #############################################################################################################
    # Exports:
    #############################################################################################################
    inofficialities_exports(input_dir, output_dirs, detected_inos_pkl_paths,
                                  docs_pkl_path, analyses_results_pkl_path,)

    logger.info("Finished.")
    return
    

#=======================================================================================================================
# Main: 
#=======================================================================================================================

if __name__ == '__main__':
    main()