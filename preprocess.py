import codecs
import pickle
from os import listdir
from sys import exc_info
from re import finditer, escape
from openpyxl import load_workbook
from collections import defaultdict
from utils import get_body_text, split_to_sentences, get_windows, eliminate_words

def preprocess(data_dir):
    """
    This is the first treatment for the raw text - Indexing.
    The text is being read file be file and sent to the text indexer.
    All this data creates the words_index and the sentences index
    which are pickled, and only their paths is being returned.  
    """
    print("Preprocessing...\nCollecting the corpus's lexicon")
    short_name = data_dir.split('/')[-2]
    data_dir_lst = listdir(data_dir)
    docs_amount = len(data_dir_lst)
    words_index = defaultdict(list)
    documents = defaultdict(list)
    for f_idx, f_name in enumerate(data_dir_lst):

        # if f_idx%1000 == 0 or f_idx == docs_amount:
        print(f"{f_idx} out of {docs_amount} docs of {short_name}. name is: {f_name}.")

        body_text = get_body_text(data_dir + "/" + f_name)
        if body_text:
            words_index, documents[f_idx] = \
                text_indexer(body_text, words_index, f_idx, f_name)

    print("eliminating")
    eliminate_words(words_index)
    print("eliminated")
    ############
    # Pickling #
    ############
    w_i_pkl_path = 'words.pkl'
    docs_pkl_path = 'docs.pkl'
    print("pickling")
    with open(w_i_pkl_path, 'wb') as w_i_pkl_file:
        pickle.dump(words_index, w_i_pkl_file, protocol=2)
    print("pickling 2")
    with open(docs_pkl_path, 'wb') as docs_pkl_file:
        pickle.dump(documents, docs_pkl_file, protocol=2)
    print("return")
    return w_i_pkl_path, docs_pkl_path

    """
    The keys of words_index are the tokens from the corpus.
    The values of those are lists of their appearances.
    Each appearance is a dictionary with these fields(for now):
      f_name = the file's name.
      f_idx = file's index in 'documents'.
      sent_idx = sentence's number in file.
      idx_in_sent = word's index in sentence.
      start = starting char position in the line.
      end = ending char position in the line.
      line_num = line number in file.
      original_token. (it will be in use after we clean the words)

    Example:
    We want to get the sentences which includes the word "Profanity":
              sentences = []
              all_appearances = words_index["Profanity"]
              for appear in all_appearances:
                  file = appear["f_idx"]
                  sent_idx = appear["sent"]
                  sentences.append(documents[file][sent_idx])

    Or in one line:
      sentences = [documents[apr["f_idx"]][apr["sent_idx"]] for apr in words_index["Profanity"]]
    """


#####################
# Helping funtions: #
#####################

def text_indexer(text, words_index, f_idx, f_name, window_size=6):
    """
    This method is in charge of collecting and storing all the details
    regarding any appearance of any word in the current file that is being read.
    It keeps track of all the index of the word and also its right and left windows.
    The words_index is being updated and returned along with the sentences of the text. 
    """
    line_num = 1
    curr_w_index = defaultdict(list)
    lines, sentences = get_lines_and_sentences(text)
    for sent_idx, sent in enumerate(sentences):
        sent_lst = sent.split(' ')
        for idx_in_sent, word in enumerate(sent_lst):
            if not word:
                continue
            line_num, start, end = \
                get_h_position(lines, line_num, word,\
                               curr_w_index[word],
                )
            appear = dict()
            appear["end"] = end
            appear["f_idx"] = f_idx
            appear["start"] = start
            appear["f_name"] = f_name
            appear["sent_idx"] = sent_idx
            appear["line_num"] = line_num
            appear["original_token"] = word
            appear["idx_in_sent"] = idx_in_sent
            appear["left_w"], appear["right_w"] = \
                get_windows(sent_lst, idx_in_sent, window_size)

            curr_w_index[word].append((line_num, start, end))
            words_index[word].append(appear)

    return words_index, sentences


def get_h_position(lines, line_num, word, prev_appears):
    """
    Getting the `human position` of the word in the text.
    Parameters
        ----------
        lines : list of strings
            That is the lines that were not entirly processed yet.
        prev_appears : list
            The previous appearances of the word.
            This is to check that we are not repeating a previous appearance.
            It is very important when a word appears twice in the same line (very common)
        line_num : int
        word : string
    """
    while line_num <= len(lines):
        current_line = lines[line_num-1]
        if word in current_line:
            for m in finditer(escape(word), current_line):
                if (line_num, m.start(), m.end()) not in prev_appears:
                    return line_num, m.start(), m.end()
        line_num += 1
    return -1, -1, -1

def get_lines_and_sentences(text):
    try:
        lines = text.split('\n')
        sentences = split_to_sentences(text)
    except MemoryError as mem_e:
        print(f'A MemoryError was raised when'/
               " spliting {f_name}'s body.")
        print(f'The message was: {mem_e}')
    except RuntimeError as run_e:
        print(f'A RuntimeError was raised when'/
               " spliting {f_name}'s body.")
        print(f'The message was: {run_e}')
    return lines, sentences


