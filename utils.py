import time
import types
import codecs
import pickle
import logging
from re import match
from sys import exc_info
from os import path, makedirs
from nltk.corpus import stopwords
from collections import defaultdict
from nltk.tokenize import sent_tokenize

def load_terms_txt(terms_path, encoding='utf-8'):
    """
    Getting the project's profanity terms archive from a .txt file,
    where the terms are separated by a new line.
    """
    plurals = list()
    bad_list = get_body_text(terms_path).replace('\r\n', '\n').split('\n')

    ##############################################
    # add the plural forms, if it seems necessary.
    for bad_term in bad_list:
        for plural_form in [bad_term + 's',bad_term + 'es']:
            if plural_form not in bad_list:
                plurals.append(plural_form)
    ##############################################

    return bad_list + plurals

def load_terms_xlsx(terms_path):
    """
    Getting the project's profanity terms archive from a .xlsx file,
    where every row of that file (besides the 1st row - the headlines)
    consist the term and after that an optional list of support words
    and optional list of reject words.
    Note: Those are words that confirm or reject the term being offensive. 
    """
    wb = load_workbook(filename=terms_path, read_only=True, sheet_name='Sheet 1')
    bingo_terms = defaultdict(list)
    ws = wb[sheet_name]
    for i, row in enumerate(ws.rows, start=1):
        if i < 3:
            continue
        word , supp_words, rej_words, *_ = row
        k = word.value
        if not k:
            continue
        bingo_terms[k] = [None, None]
        for i, val in enumerate((supp_words.value, rej_words.value)):
            if val:
                # While these are only for the profanity check,
                # they are not case-sensitive. 
                bingo_terms[k][i] = [w.lower() \
                        for w in val.replace(', ', ',').split(',')]
    return bingo_terms

def most_n_common_words(words_index, n):
    """
    This method returns the words with the most appearances in the corpus.
    """
    return sorted(words_index.items(), \
        key=lambda kv: len(kv[1]), reverse=True)[:n]

def should_ignore(token, max_token_len=80):
    """
    This method finds tokens that are relatively big,
    but probably not consist anything offensive,
    so ignoring them in an early stage might save a precious time.
    It ignores links, paths, email signature fields and numbers (including money and times).
    
    NOTE: consider ignore names as well, by:
        return True (if token is a name) else False. 
    """
    if len(token) > max_token_len:
        return True
    if match(r'(?<!\S)\d(?!\S)', token):
        return False
    regexes = [
        r'.+/.+',
        r'(ww\..*)',
        r'mailto:(.*)',
        r'https?:\/\/(.*)',
        r'^[:.\-+,$]?\d+(?:[:.\-+,]\d+)*(?:[apkcmf]m|%|m|\$)?$',
        # the last regex checks if the token is a number/hour (with or w/o units).
    ]
    return True if match("|".join(regexes), token) else False

def eliminate_words(w_i_pkl_path, elimination_amount=1000):
    """
    This method is in charge of reducing the words_index (the one in the pickle path)
    amount by eliminating troublesome tokens that will consist no offensiveness.
    These are stop words or the most common words (a curse word will probably not be
    one of the most repeated words in the corpus), or other types that are detected
    by 'should_ignore(token)' method.
    """
    words_index = w_i_pkl_path
    if type(w_i_pkl_path) is str:
        # first unpickle:
        with open(w_i_pkl_path, 'rb') as w_i_pkl_file:
            words_index = pickle.load(w_i_pkl_file)
    to_eliminate = most_n_common_words(words_index, n=elimination_amount)
    stop_words = set(stopwords.words('english'))
    for word in list(words_index):
        if should_ignore(word) or word in to_eliminate \
                               or word in stop_words:
            del words_index[word]
    # pickle back:
    if type(w_i_pkl_path) is str:
        with open(w_i_pkl_path, 'wb') as w_i_pkl_file:
            pickle.dump(words_index, w_i_pkl_file)
        return
    return words_index

def lower_case_index_keys(w_i_pkl_path):
    """
    This method changes the original-cased words_index to a lower case one.
    """
    with open(w_i_pkl_path, 'rb') as w_i_pkl_file:
        words_index = pickle.load(w_i_pkl_file)    
    lower_words_index = defaultdict(list)
    for word, appears in words_index.items():
        if appears:
            lower_words_index[word.lower()] += appears
    with open(w_i_pkl_path, 'wb') as w_i_pkl_file:
        pickle.dump(lower_words_index, w_i_pkl_file)
    return

def split_to_sentences(text):
    """
    This methods splits the given text into sentences,
    ignoring the empty lines and returns a list of these sentences.
    """
    all_sentences = list()
    for token in sent_tokenize(text):
        sentences = token.replace('\t', ' ') \
                          .replace('\r\n', '\n') \
                          .replace(',\n', ', ').split('\n')
        all_sentences += [sent for sent in sentences if sent]
    return all_sentences

def get_body_text(fname, encodings=['utf-8-sig', 'utf-16', 'utf-16-le', 'utf-16-be']):
    """
    This method is reading the given file and returns the text.
    The decoding is being made based on the given list of encodings.
    If the method fails to decode with the first one, it continues to
    try the next one on the list, and so on. A total fail returns None.

    Note: the order is important! We wish to decode the file by 'utf-8'.
    """
    for en in encodings:
        try:
           with codecs.open(fname, 'r', encoding=en) as rf:
                return rf.read()
        except UnicodeDecodeError as e:
            # do nothing and check next encoding
            continue
        except Exception as e:
            exc_type, _, exc_tb = exc_info()
            print('An exception in get_body_text: (not a UnicodeDecodeError!)')
            print(f"Exception type is {exc_type},")
            print(f'and the exception occurred in line number {exc_tb.tb_lineno}')
        else:
            break
    return None


def get_windows(body_words_lst, word_idx, size=6):
    """
    Getting the left and right windows of word number "word_idx" in "body_text".
    Parameters
    ----------
    body_words_lst : list
    word_idx : int
    size : int
        This is the max size of the wanted returned windows.

    Returns
    -------
    left_window : str
        a "size" amount of words in the "body_text" that came right BEFORE the word number "word_idx" there.
    right_window : str
        a "size" amount of words in the "body_text" that came right AFTER the word number "word_idx" there.
        
    """
    most_left = max(0, word_idx - size)
    l_win_lst = body_words_lst[most_left:word_idx]
    
    most_right = min(word_idx + size, len(body_words_lst))
    r_win_lst = body_words_lst[word_idx + 1:most_right + 1]

    left_window = ' '.join(word for word in l_win_lst)
    right_window = ' '.join(word for word in r_win_lst)

    return left_window, right_window


def merge_defaultdicts(d1, d2):
    d3 = defaultdict(list)
    for k1,v1 in d1.items():
        d3[k1] = v1 
    for k2,v2 in d2.items():
        d3[k2] += v2
    return d3

########################################################################################
######################## ----------- Logging stuff ------------ ########################
########################################################################################

def get_logging_handlers():
    """Configures and returns file and console logging handlers"""
    # check if logs directory exists, if not create one
    if not path.exists("logs"):
       makedirs("logs")
        
    start_time = time.strftime("%Y-%m-%d-%H-%M")      
    log_file = path.join('logs', start_time + ".log")
    
    formatters = {
        "file": logging.Formatter(
          '%(message)s' #'%(asctime)s - %(name)s - %(levelname)s - %(message)s'
          ),
        "console": logging.Formatter(
          '%(asctime)s - %(name)s - %(message)s',
          "%Y-%m-%d %H:%M:%S"
          )
    }
    
    # add file handler
    fh = logging.FileHandler(log_file)
    fh.setFormatter(formatters['file'])
    fh.setLevel(logging.DEBUG)
    # add console handler
    ch = logging.StreamHandler()
    ch.setFormatter(formatters['console'])
    ch.setLevel(logging.INFO)

    bh = logging.StreamHandler()
    bh.setLevel(logging.INFO)
    bh.setFormatter(logging.Formatter(fmt=''))
    
    return fh, ch, bh

def log_newline(self, how_many_lines=1):
    # Switch handler, output a blank line
    self.removeHandler(self.ch)
    self.addHandler(self.bh)
    for i in range(how_many_lines):
        self.info('')

    # Switch back
    self.removeHandler(self.bh)
    self.addHandler(self.ch)

def create_logger(name, level):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    fh, ch, bh = get_logging_handlers()
    logger.bh = bh
    logger.ch = ch
    logger.fh = fh
    logger.addHandler(fh)
    # logger.addHandler(ch)
    logger.newline = types.MethodType(log_newline, logger)
    return logger



