import pickle
from collections import defaultdict
from utils import should_ignore as should_ignore_by_regexps

"""
This utils file helps in two places:
1) The punctuation cleaning part, that is being called from the main app.py file.
2) The determination of the inofficiality of a punctuation, that is being called
   from the punctuation lookup.
"""

_punct = (
    '…', '~', '”', '“',
    ',', '.', '?', '!',
    ';', ':', '‘', '’',
    "'", '"', '\\', '/',
    '(', ')', '[', ']',
    '}', '{', '>', '<',
    '-', '_', '|', '`',
    )

_shouting_punct = (
    '?', '!',
    )

_parentheses = {'(':')', '[':']',
                '{':'}', '<':'>',
}

_punct_length = lambda tup: len(tup[0]) + len(tup[1])

def is_parenthesis(punct_tup):
    pref, suff = punct_tup
    if pref[0] in _parentheses:
        return _parentheses[pref[0]] == suff[-1] # checking the outer chars 
    return False

def should_ignore_punct(punct_tup):
    '''
    this method checks if we should consider a punctuation as a complete non-inofficial.
    For now it is saying that only if the punctuation was really short, or just a nested parentheses.
    '''
    punct_len = _punct_length(punct_tup)
    if punct_len < 2: return True
    if is_nested_parentheses(punct_tup): return True
    return False

def is_nested_parentheses(punct_tup):
    """
    This method returns True if the given punctuation is a nested parentheses.
    In every iteration it checks the outer chars, and if they constitute a parentheses
    it 'peels' them and to the next iteration. Else, return False.
    """
    pref, suff = punct_tup
    if len(pref) != len(suff): return False
    while pref:
        if not is_parenthesis(punct_tup): # cheacking the outer puncts
            return False
        # removing the outer puncts. i.e., going one layer deeper
        pref = pref[1:]
        suff = suff[:-1]
    return True

def token_cleaner(token, record_puncts=True):
    """
    cleanning the given token from unnecessary punctuations and other kinds of noises.
    """
    token, suffix = clean_suffix(token)
    token, prefix = clean_prefix(token)
    surr_punct = (prefix, suffix) if record_puncts else None

    return reduce_repetitions(token), surr_punct

def surrounded_punct_is_shouting(surr_punct):
    '''
    Returns True if the puncuation consist more than 2 chars. Else, False.
    '''
    return len([c for c in surr_punct if c in _shouting_punct]) > 2


def reduce_repetitions(token, max_repetitions=2):
    """
    reduce alpha-beth letters repetitions to 2 max in the given token.
    """
    rep_counter = 0
    last_letter = ""
    cleaned_token = ""
    for letter in token:
        if letter == last_letter and letter.isalpha():
            rep_counter += 1
            if rep_counter < max_repetitions:
                cleaned_token += letter
        else:
            cleaned_token += letter
            rep_counter = 0
        last_letter = letter
    return cleaned_token

###############################################################################
# In the next 2 cleaning methods,
# consider changing them to a regex use (by using the regex's .group() method).
###############################################################################

def clean_prefix(token):
    """
    clean the unnecessary punctuations and other kinds
    of noise from the BEGINNING of the given token.
    """

    prefix_punct = ''
    while token != '':
        if token.startswith(_punct): # _punct is a tuple of 1-char punctuations.
            prefix_punct += token[0]
            token = token[1:]
            continue
        break
    return token, prefix_punct


def clean_suffix(token):
    """
    clean the unnecessary punctuations and other kinds
    of noise from the END of the given token.
    """
    suffix_punct = ''
    while token != '':
        if token.endswith(_punct): # _punct is a tuple of one-char punctuations.
            suffix_punct = token[-1] + suffix_punct
            token = token[:-1]
            continue
        if token.endswith(("'s","'t","’s","’t")):
            token = token[:-2]
        if token.endswith(("'nt","’nt","'ll","’ll")):
            token = token[:-3]
        return token, suffix_punct
    return token, suffix_punct # if for some reason, the recieved/cleaned token was just an empty string - ''.



###################################################################
###################################################################


def punct_cleaning(w_i_pkl_path, record_punct=True):
    '''
    This method is cleaning the raw tokens from their punctuations.
    If the punctuation seem long enough and not something that could
    be immediately ignored (like nested parentheses), it is being documented
    and will be reviewed later, in the punctuation lookup part.
    
    Also, the cleaned tokens are then can be considered as words and as such,
    they are being collect to a words_index.

    # Note:
    The punctuations could be reviewed here already, but are being saved for
    later because if the design and the important value of modularity and independence.
    '''
    all_puncts_path = "punctuation/all_puncts.pkl"
    with open(w_i_pkl_path, 'rb') as w_i_pkl_file:
        token_index = pickle.load(w_i_pkl_file)
    all_puncts = defaultdict(list)
    words_index = defaultdict(list)
    tokens_amount = len(token_index)
    for i, (original_tk, appears) in enumerate(token_index.items(), start=1):

        print("cleaning punct of token ", i, " out of ", tokens_amount)

        cleaned_tk, punct_of_tk = token_cleaner(original_tk, record_punct)
        if appears and 3 < len(cleaned_tk) < 100 \
                   and not should_ignore_by_regexps(cleaned_tk):
            # Fixing the token_index:
            for appear in appears:
                appear["clean_of_punct"] = cleaned_tk
                words_index[cleaned_tk].append(appear)
                # NOTE: the saved "appear" is a dictionary that now contains the fields: 
                # {f_name ,f_idx ,sent_idx, idx_in_sent, start, end ,line_num, original_token, clean_of_punct} 

        if should_ignore_punct(punct_of_tk): continue
        
        stands_alone = False if cleaned_tk else True
        # a punctuation is considered as standing alone if the whole original token was just that punctuation alone.

        # Collecting the found punctuation:
        for appear in appears:
            appear["stands_alone"] = str(stands_alone)
            appear["cleaned"] = cleaned_tk
            all_puncts[punct_of_tk].append(appear)
            # NOTE: the saved "appear" is a dictionary that now contains the fields: 
            # {f_name ,f_idx ,sent_idx, idx_in_sent, start, end ,line_num, original_token, cleaned, stands_alone} 

    # pickle
    with open(w_i_pkl_path, 'wb') as w_i_pkl_file:
        pickle.dump(words_index, w_i_pkl_file)
    with open(all_puncts_path, 'wb') as all_punct_pkl_file:
        pickle.dump(all_puncts, all_punct_pkl_file)

    return all_puncts_path
