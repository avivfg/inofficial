import pickle
from collections import defaultdict
from punctuation.punctuation_utils import surrounded_punct_is_shouting

"""
"Punctuation" - detection of exaggerated punctuations.
    As a part of the "inofficial" project, this task have to take its role
    before the words_index is getting cleaned of the punctuations , of course.
    It means that it is probably the first task that ino will do,
    after creating the words_index.

    Note:
    -----
    all_puncts_path is the path to the collection of all the punctuations.
    The punctuations were already got pulled from the tokens in words_index
    (as a 2-tuple of the "prefix-punctuation" and the "suffix-punctuation").
    This system runs through all of them, and document them if they are iofficial.

    Note!
    -----
    The part that determine if the punctuation should be regarded as inofficial
    (or a Shouting) is located at the punctuation_utils.py file,
    and so any change in the way of the determination should take place there. 
    Just follow the function "is_inofficial(punct_tup)".
"""

def find_ino_punct(all_puncts_path, docs_pkl_path):
    detected_puncts_path = "punctuation/detected_puncts.pkl"
    with open(docs_pkl_path, 'rb') as docs_pkl_file:
        documents = pickle.load(docs_pkl_file)
    with open(all_puncts_path, 'rb') as all_puncts_pkl_file:
        puncts = pickle.load(all_puncts_pkl_file)
    label = ''
    score = 100
    puncts_amount = len(puncts)
    detected = defaultdict(list)
    print('\n##################\n\nChecking the punct lexicon...')
    for i, punct in enumerate(puncts, start=1):
        print(i, " out of ", puncts_amount, " puncts ------ (Punctuation)")
        if is_inofficial(punct):
            for appear in puncts[punct]:
                appear["label"] = label
                appear["score"] = score
                detected[punct].append(appear)
                
    ##################################################################################
    # The detected Punctuations are documented just like the words_index's appearances,
    # with extra particular details - and eventually looks like that: (fields names) 
    # {f_name ,f_idx ,sent_idx, idx_in_sent, start, end ,line_num, original_token,
    #  clean_of_punct, stands_alone, score, label}
    ##################################################################################

    with open(detected_puncts_path, 'wb') as detected_punct_pkl_file:
        pickle.dump(detected, detected_punct_pkl_file)
    return detected_puncts_path



def is_inofficial(punct_tup):
    '''
    This method gets a punct-tuple, combines it into one String and sends it to determination.
    '''
    return surrounded_punct_is_shouting(''.join(punct_tup))