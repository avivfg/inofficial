from operator import itemgetter
from openpyxl import load_workbook
from collections import defaultdict

def contains_alpha(word):
    """
    True if there is A alpha-beth char in "word". else, False.
    """
    return any(char.isalpha() for char in word)

def load_terms_xlsx(terms_path, sheet_name='Sheet 1'):
    """
    Getting the project's profanity terms archive from a .xlsx file,
    where every row of that file (besides the 1st row - the headlines)
    consist the term and after that an optional list of support words
    and optional list of reject words.
    Note: Those are words that confirm or reject the term being offensive. 
    """
    wb = load_workbook(filename=terms_path, read_only=True)
    bingo_terms = defaultdict(list)
    ws = wb[sheet_name]
    for i, row in enumerate(ws.rows, start=1):
        if i < 3:
            continue
        word , supp_words, rej_words, *_ = row
        k = word.value
        if not k:
            continue
        bingo_terms[k] = [None, None]
        for i, val in enumerate((supp_words.value, rej_words.value)):
            if val:
                # While these are only for the profanity check,
                # they are not case-sensitive. 
                bingo_terms[k][i] = [w.lower() \
                        for w in val.replace(', ', ',').split(',')]
    return bingo_terms
def merge_defaultdicts(d1, d2):
    """
    merging two defaultdicts into one.
    """
    d3 = defaultdict(list)
    for k1, v1 in d1.items():
        d3[k1] = v1 
    for k2, v2 in d2.items():
        d3[k2] += v2
    return d3

def k_most_valuable_keys(a_dict, k=10):
    """
    This method returns a sorted list of the k keys that had the highest value.
    """
    sorted_k_most_valuable = sorted(a_dict.items(), key=itemgetter(1), reverse=True)[:k]
    return [key for (key, val) in sorted_k_most_valuable]

