import pickle
from collections import defaultdict
from .profanity_utils import load_terms_xlsx
from .similarity_check import compare_pair_of_words, compare_next_words, token_is_around_idx

"""
'Profanity' - detection of profanities.
    As a part of the "inofficial" project, this task have to take its role
    after the words_index is going through a punctuation cleaning and even being lower-cased.
"""

def lookup(words_index_path, documents_path):
    '''
    This method loads the bingo terms and then trying to find them through the indexed corpus.
    Returns the path of the pickled results back to the main app.py file.
    '''
    bingo_terms = load_terms_xlsx("profanity/bingo_words.xlsx")
    # bingo_words.xlsx is a table that consist the bad terms along
    # with their supporting words and rejecting words (if they got any).

    # now we can go through the words in words_index and check them by the bingo terms' table:
    return find_bingo_words(words_index_path, documents_path, bingo_terms)    


def find_bingo_words(w_i_pkl_path, docs_pkl_path, bingo_terms):
    '''
    This profanity lookup is searching the appearances of the bingo terms throughout the corpus.
    It is checking the similarity of the words in the corpus to the terms themselves,
    while the bingo terms' rejecting and supporting words are also being checked,
    to reject or support the similarity check.
    '''

    # unpickle:
    detected_profs_path = "profanity/detected_profs.pkl"
    with open(w_i_pkl_path, 'rb') as w_i_pkl_file:
        index = pickle.load(w_i_pkl_file)
    with open(docs_pkl_path, 'rb') as docs_pkl_file:
        documents = pickle.load(docs_pkl_file)

    words_amount = len(index)
    detected_profanities = defaultdict(list)
    print('\n##################\n\nChecking the lexicon for profanities...')
    for i, (word, appearances) in enumerate(index.items(), start=1):
        print(i, " out of ", words_amount, " clean lowercase words ------ (Profanity)")
        for p_word, [supp_words, rej_words] in bingo_terms.items(): 

            # In case that a profanity term consist more than one word,
            # we want to check the first word first, then check rejecting and supporting word.
            p_word_lst = p_word.split()
            first_is_similar, first_ratio = compare_pair_of_words(word, p_word_lst[0])
            if not first_is_similar:
                continue

            # First word matched. The next checks have to be performed over each appearance separately:
            for apr in appearances:
                sentence = documents[apr["f_idx"]][apr["sent_idx"]]
                apr["support_words"] = ''
                apr["label"] = ''

                #################
                # Rejection part:
                #################
                if rej_words:
                    if any(token_is_around_idx(sentence, rej, apr["idx_in_sent"]) \
                                                            for rej in rej_words):
                        continue #  = rejecting

                #########################
                # Supporting words check:
                #########################
                if supp_words:
                    for supp in supp_words:   
                        if token_is_around_idx(sentence, supp, apr["idx_in_sent"]):
                            apr["support_words"] = supp
                            apr["label"] = 'supported'
                            break

                ##################################################################
                # If a word survived this far, it means that the first word of the
                # profanity term matched and was not rejected.
                # The only thing that can fail it is if the profanity term consist
                # more words that are not matched. This is the last check:
                ##################################################################

                are_similar, apr["quote"], apr["score"] = \
                        compare_next_words(word, p_word_lst, sentence,
                                           apr["idx_in_sent"], first_ratio,)
                if are_similar:
                    detected_profanities[p_word].append(apr)
        ##################################################################################
        # The detected Profanities are documented just like the words_index's appearances,
        # with extra particular details - and eventually looks like that: (fields names) 
        # {f_name ,f_idx ,sent_idx, idx_in_sent, start, end ,line_num,
        #  original_token, clean_of_punct, supported_word, label, score, whole_quote}
        ##################################################################################

    # pickle:
    with open(detected_profs_path, 'wb') as detected_profs_pkl_file:
        pickle.dump(detected_profanities, detected_profs_pkl_file)
    return detected_profs_path 

