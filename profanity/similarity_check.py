from fuzzywuzzy import fuzz
from .profanity_utils import contains_alpha

"""
This file's functions are specific utilities for the task of checking
similarities between words and terms. 
"""


def compare_next_words(word, p_term_lst, sentence,
                       word_idx_in_sent, first_ratio):
    """
    This method is in charge of checking the rest of the words of the profanity term,
    after the first one was approved.

    Arguments
    ---------
    word : string
        the first word of the profanity term (that was already matched)
    p_term_lst : list of strings
        the profanity term, word-separated into a list.
    sentence : string
        the current appearance's sentence.
    first_ratio : int
        the ratio of certainty of the first word's similarity.
        we need it only to calculate the total ratio of similarity of all the term, eventually. 

    Returns
    -------
    are_similar : bool
        True if the whole quote (starting with "word") is similar to the p_term. else, False.
    quote : string
        the whole quote from the text (starting with "word"). If the test failed - it is None.
    total_ratio(ratios) : int
        The average of all the ratios. None if the test fails.
    """
    p_len = len(p_term_lst)
    if p_len == 1:
        return True, word, first_ratio
    next_words = sentence.split()[word_idx_in_sent+1:]
    next_words_amount = len(next_words)
    if p_len > next_words_amount:
        return False, None, None

    next_words = next_words[:p_len]
    ratios = list()
    ratios.append(first_ratio)
    quote = word
    for p_word_idx in range(p_len - 1):
        p_word = p_term_lst[p_word_idx + 1]
        next_word = next_words[p_word_idx + 1]
        sim, ratio = compare_pair_of_words(next_word, p_word)
        if not sim:
            return False , None, None
        ratios.append(ratio)
        quote += " " + next_word
    return True, quote, total_ratio(ratios)


def check_similarity(word, p_word):
    """
    check similarity between 2 words. The check if different for each word_obsness.
    ----------
    word : string
        A word from the text.
    p_word : string
         A word from some profanity term.
    word_obsness : string
        The word's obscureness: "totally-revealed" or "partially-obscured" or "totally-obscured"

    Returns
    -------
    ratio : int
        The ratio between the 2 words. If the test failed - it is 0.

    """
    if word == p_word:
        return 100
    if len(word) != len(p_word):
        return 0
    word_obsness = check_obscureness(word)
    if word_obsness == "totally-revealed":
        return check_revealed_word(word, p_word)
    elif word_obsness[0] == 'p':
        return check_pobs_word(word, p_word) # word_obsness == "partially-obscured"
    return check_tobs_word(word, p_word) # word_obsness == "totally-obscured"

def token_is_around_idx(sentence, token, idx_in_sent):
    '''
    This method checks if the given token is found close enough to a word in index idx_in_sent in the sentence.
    '''
    tok = token.split(' ')
    sent = sentence.split(' ')
    if not all(w in sent for w in tok):
        return False
    tok_last_w = tok[-1]
    tok_first_w = tok[0]
    close_enough = lambda s, w : abs(s.index(w) - idx_in_sent) < 4 
    return close_enough(sent, tok_first_w) or close_enough(sent, tok_last_w) 


def check_obscureness(word):
    """
    returns the word's obscureness.

    Returns
    -------
    "totally-revealed" (example - "ass"),
    or "partially-obscured" (example - "@ss"),
    or "totally-obscured" (example - "@$$").
    """
    if contains_alpha(word):
        if word.isalpha():
            return "totally-revealed"
        return "partially-obscured"
    return "totally-obscured"

def compare_pair_of_words(word, first_p_word, threshold=90):
    """
    checks the similarity of a word from the text and the 1st word of a profanity-term.

    Parameters
    ----------
    word : string
        A token from the text.

    Returns
    -------
    """
    ratio = check_similarity(word, first_p_word)
    return ratio > threshold, ratio

def check_revealed_word(word1, word2):
    """
    check similarity between 2 revealed words.
    (a "revealed word" is a token that contains ONLY alpha-beth chars)
    """
    return fuzz.ratio(word1, word2)


def check_pobs_word(word, p_word):
    """
    check similarity between a partially-obscured word and the p_word.
    (a "partially-obscured" is a token that contains alpha-beth chars AND non alpha-beth chars too)
    """
    return similar_under_transformation(word, p_word)


def check_tobs_word(word, p_word):
    """
    check similarity between a "totally-obscured" word and the p_word.
    (a "totally-obscured" is a token that contains ONLY non alpha-beth chars too)
    """

    # needs to be implemented.
    # by now the only option we got is to check if it is translates to a curse word with the transformation.
    return similar_under_transformation(word, p_word)
    # this is actually the most difficult one to determine, because it could be any non-alpha noise!!


def total_ratio(ratios):
    """
    Gives the average of the ratios.
    """
    return sum(ratios) / len(ratios) if len(ratios) > 0 else 0

def signs_to_letters(token,trans_dict):
    """
    Converts "token" (which should contain some symbols) to a total alpha-beth token.
    """
    converted = ''
    for char in token:
        if char.isalpha() or char not in trans_dict:
            converted += char
            continue
        conv_char = trans_dict[char]
        converted += conv_char
    return converted

def share_revealed_letters(word1,word2):
    """
    True if the 2 given words shares their revealed letters. else, False.
    """
    for char_of_word_1, char_of_word_2 in zip(word1, word2):
        if char_of_word_1 != char_of_word_2 and \
                    char_of_word_1.isalpha() and \
                    char_of_word_2.isalpha():
            return False
    return True

def similar_under_transformation(word1, word2):
    """
    Checks if 2 words (that should contain non alpha-beth chars) shares letters,
    and if changing their symbols to letters gives 2 similar enough words (including a penalty for that). 

    Returns
    -------
    ratio : int
        The percentage (75%-95%) of similarity if the words where actually close to each other.
        0 if they were not.
    """
    penalty = 0.95
    trans_dict = {
        "@": "a",
        "$": "s",
        "#": "h",
        "+": "t",
        "!": "i",
        "1": "l",
        "2": "z",
        "3": "e",
        "5": "s",
        "6": "b",
        "7": "t",
        "8": "b",
        "9": "g",
        }
    if not share_revealed_letters(word1,word2):
        return 0
    return fuzz.ratio(
        signs_to_letters(word1,trans_dict),
        signs_to_letters(word2,trans_dict)
        ) * penalty