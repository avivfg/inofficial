import nltk
from operator import itemgetter
from collections import defaultdict, Counter
from .profanity_utils import k_most_valuable_keys
from nltk.collocations import BigramCollocationFinder
from nltk.collocations import TrigramCollocationFinder
# from nltk.collocations import QuadgramCollocationFinder


"""
This is the profanity analysis file - also known as the `Collocations` file.
These functions helps us get information about the collocations that were involved
with the profanity appearances that were found by the profanity lookup.
This may give us more knowledge about the behaviors of the profanities
and when can we inspect them to be.    
"""

def get_collocations_dict(bad_words_found, documents,
                          dedup_ino_counter, fav_terms_only=True):
    '''
    This method gets the results of the profanity lookup and returns
    a dictionary with info about the n-grams that were the most involved
    with the found profanities. 

    Note:
    fav_terms_only is a flag the say that we are going to check the collocations
    Only around the terms that had the most appearances.
    '''
    collocations_dict = dict()
    (bigrams_data, trigrams_data) = \
                        get_ngrams(bad_words_found, documents,
                                   dedup_ino_counter, fav_terms_only)
    collocations_dict['bigrams'] = bigrams_data
    collocations_dict['trigrams'] = trigrams_data

    return collocations_dict



def get_fav_ngrams(documents, hot_terms, bad_words_found):
    '''
    This method collects the sentences where each hot term appeared,
    and sends them to the actual collocations calculating part.
    Finally, it is sending them to a function that updating this
    collection of sentences, to those who consist the wanted collocations,
    so we can have examples for the sentences they have appeared in.
    This final product is the returned value. 
    '''
    bigrams = defaultdict(list)
    trigrams = defaultdict(list)
    sents_per_fav = defaultdict(list)

    # collecting the sentences of the most appeared profanities.
    for term in hot_terms:
        for appear in bad_words_found[term]:
            sents_per_fav[term].append(documents[appear["f_idx"]][appear["sent_idx"]])

    for fav in hot_terms:

        # deduplicating the sentences of each hot term, and store them as lists of words.
        sents_per_fav[fav] = [s.split() for s in set(sents_per_fav[fav])]

        # getting the collocations:
        bigrams[fav] = get_bigram_collocations([fav], sents_per_fav[fav], False)[fav]
        trigrams[fav] = get_trigram_collocations([fav], sents_per_fav[fav], False)[fav]

    return update_ngram_and_get_examples(bigrams, sents_per_fav, hot_terms), \
           update_ngram_and_get_examples(trigrams, sents_per_fav, hot_terms)

def update_ngram_and_get_examples(ngrams, sents_per_term, terms):
    '''
    this method gets n-grams and, collect the sentences that each one appeared in,
    then returns those who did appeared along with one of the given terms
    and their sentences. 
    '''
    updated_ngrams = defaultdict(list)
    ngrams_examples = defaultdict(list)
    for term in terms:
        for (score, coll) in ngrams[term]:
            for sentence in sents_per_term[term]:
                if any((word == term or word not in sentence) for word in coll) \
                             or term not in sentence:
                    continue 
                if coll_is_in_sentence(term, coll, sentence):
                    ngrams_examples[(term, coll)].append(sentence)
                    if (score, coll) not in updated_ngrams[term]:
                        updated_ngrams[term].append((score, coll))
    return updated_ngrams, ngrams_examples

def coll_is_in_sentence(term, coll, lst_sentence):
    '''
    This method return True if all the words of the collocations were close enough to the given term.
    '''
    for term_idx in get_indices(term, lst_sentence):
        if all(
                any(abs(word_idx - term_idx) <= 5 \
                    for word_idx in get_indices(word, lst_sentence)) \
                                            for word in coll):
            return True
            # NOTE:
            # It means, return True *if* there is an occurrence of the term in the sentence,
            # that *every* word in the collocations have an appearance close enough to it.
    return False


def get_indices(word, lst_sentence):
    '''
    This method returns all the indexes where the given word was found in the given sentence.
    '''
    return [idx for idx, w in enumerate(lst_sentence) if w == word]



def get_ngrams(bad_words_found, documents, dedup_p_counter, fav_only=True):
    '''
    This method returns the n-grams of the profanity terms that were found in the corpus.
    If fav_only is True,
    it means that it is being done to the most appeared terms (= hot_terms = favorites = `fav`) only.
    '''
    if fav_only: # check the collocations of the leading profanities only.
        hot_terms = k_most_valuable_keys(dedup_p_counter)
        return get_fav_ngrams(documents, hot_terms, bad_words_found)

    # else, we check the collocations of every found profanity term that was found:
    bigrams = defaultdict(list)
    trigrams = defaultdict(list)
    bigrams = get_bigram_collocations(bad_words_found.keys(), documents)
    trigrams = get_trigram_collocations(bad_words_found.keys(), documents)
    return bigrams, trigrams



def get_bigram_collocations(p_terms, documents, exclusive=True):
    '''
    This method uses BigramCollocationFinder to initiate the bigrams calculation.
    '''    
    BigramCollocationFinder.default_ws = 9
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_documents(documents)
    return get_candidates(finder, bigram_measures, p_terms, exclusive)

def get_trigram_collocations(p_terms, documents, exclusive=True):
    '''
    This method uses TrigramCollocationFinder to initiate the tigrams calculation.
    '''
    TrigramCollocationFinder.default_ws = 9
    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    finder = TrigramCollocationFinder.from_documents(documents)
    return get_candidates(finder, trigram_measures, p_terms, exclusive)

def get_candidates(finder, ngram_measures, p_terms, exclusive=True):
    '''
    This method goes through the n-grams and their scores,
    and narrows them down to the most relevant, by removing those
    who were too short, contained stop-words, didn't contained the term, and so on.
    This method returns the candidates to be the collocations.
    '''
    colls = defaultdict(list)
    score_fn = ngram_measures.likelihood_ratio
    ignored_words = nltk.corpus.stopwords.words('english')
    word_filter = lambda w: len(w) < 3 or w.lower() in ignored_words
    finder.apply_word_filter(word_filter)
    scores = get_scores(finder, score_fn)
    for p_term in p_terms:
        
        ino_term = p_term
        if type(p_term) is tuple:
            ino_term = ''.join(p_term) 

        for (ngram, score) in scores:
            if not exclusive: # not necessarily contains the ino_term.
                if (score, ngram) not in colls[p_term]:
                    colls[p_term].append((score, ngram))

            if any((w.endswith('.') and w != ino_term + '.') for w in ngram):
                continue
            if any((w == ino_term or w == ino_term + '.') for w in ngram) \
                    and (score, ngram) not in colls[p_term]:
                colls[p_term].append((score, ngram))
    return colls
    # colls = finder.nbest(bigram_measures.likelihood_ratio, amount)


def get_scores(finder, score_fn):
    '''
    Returns the scores of the n-gram
    '''
    return sorted(finder._score_ngrams(score_fn), key=lambda t: (-t[1], t[0]))
