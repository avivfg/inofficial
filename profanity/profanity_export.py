from operator import itemgetter
from xlsxwriter import Workbook
from collections import defaultdict, Counter
from .profanity_utils import merge_defaultdicts, k_most_valuable_keys

"""
This file is in charge of exporting the specific analysis of the profanity detection,
which is the collocations analysis (for now).
"""

def export_profanities(input_dir, output_dir, bad_words_found,
                       documents, analysis_results, fav_only=True):
    '''
    This is the main method that calls the different exports.
    '''
    excel_output_dir = output_dir + "/excel output files"

    freq_dict = analysis_results['frequencies']
    collocations_dict = analysis_results['collocations']

    print("writing Collocations...")
    write_collocations(excel_output_dir, collocations_dict,
                       freq_dict['dedup_ino_counter'], fav_only)
    return

def write_collocations(output_dir, collocations_dict, dedup_p_counter, fav_only):
    '''
    This method is in charge of ordering to write the collocations.
    If they were not picked by the favorite terms only, then  
    '''
    bigrams, bigrams_examples = collocations_dict['bigrams']
    trigrams, tigrams_examples = collocations_dict['trigrams']
    if fav_only:
        fname = '/Hot_terms_collocations'
        write_ngrams(bigrams, bigrams_examples, fname, output_dir, dedup_p_counter, 'bigrams')
        write_ngrams(trigrams, tigrams_examples, fname, output_dir, dedup_p_counter, 'trigrams')
        return
    # else: 
    fname = '/collocations'
    ngrams = merge_defaultdicts(bigrams, trigrams)
    ngrams_examples = merge_defaultdicts(bigrams_examples, tigrams_examples)
    write_ngrams(ngrams, ngrams_examples, fname, output_dir, dedup_p_counter)
    return

def write_ngrams(ngrams, examples, fname, output_dir, dedup_p_counter, sheet_name='n-grams'):

    headers = ['Hot-term', 'Collocation (and examples)', 'Score', 'Label']

    with Workbook(output_dir + fname + '_' + sheet_name + '.xlsx') as workbook:

        worksheet = workbook.add_worksheet(sheet_name)
        str_format = workbook.add_format({'align': 'left', 'text_wrap': 4, 'align': 'top'})
        coll_format = workbook.add_format({'bg_color': '#d3e2ff', 'border': 1, 'align': 'top'})
        score_format = workbook.add_format({'bg_color': '#f9ff59', 'border': 1})
        term_format = workbook.add_format({'bg_color': '#ffc1f2','bold': 1, \
                                           'border': 1, 'font_size': 12,
        })
        sub_term_format = workbook.add_format({'bg_color': '#ffcc99', \
                                               'bold': 1, 'border': 1, \
                                               'align': 'top', 'font_size': 11,
        })
        header_format = workbook.add_format({'font_size': 12,
                                               'align': 'center',
                                               'bg_color': '#C9FFE5',
                                               'bold': 1, 'border': 1,  
        })
        
        longest_term_size = 0
        for fav in k_most_valuable_keys(dedup_p_counter):
            if len(fav) > longest_term_size:
                longest_term_size = len(fav)
        worksheet.set_column(0, 0, max(len(headers[0]), longest_term_size))
                
        widths_dict = Counter()
        for fav_term, fav_term_colls in ngrams.items():
            for i, (score, coll) in enumerate(sorted(fav_term_colls, \
                                    key=itemgetter(0), reverse=True)):
                tup_width = 0
                for word in coll:
                    tup_width += len(str(word))+2
                tup_width = max(len(headers[1]), tup_width)
                score_width = max(len(headers[2]), len(str(score)))
                widths_dict[1] = max(widths_dict[1], tup_width)
                widths_dict[2] = max(widths_dict[2], score_width)
        for j in widths_dict:
            if j > 0:
                worksheet.set_column(j, j, widths_dict[j])

        for k ,header in enumerate(headers):
            worksheet.write_string(0, k, headers[k], header_format)
        row = 1
        for fav_term, fav_term_colls in ngrams.items():
            worksheet.write_string(row, 0, fav_term, term_format)
            for i, (score, coll) in enumerate(sorted(fav_term_colls, \
                                    key=itemgetter(0), reverse=True)):

                worksheet.write_string(row, 0, fav_term, term_format)
                worksheet.write_string(row, 1, str(coll), coll_format)
                worksheet.write_string(row, 2, str(score), score_format)
                row += 1

                examples_amount = min(len(examples[(fav_term, coll)]), 10)
                examples_display = examples[(fav_term, coll)][:examples_amount]

                for ex_idx, example in enumerate(examples_display, start=1):
                    worksheet.write_string(row, 0, fav_term, sub_term_format)
                    worksheet.write_string(row, 1, '-' + str(coll) \
                                       + ' \nExample ' + str(ex_idx) + ') ', str_format)
                    worksheet.write_string(row, 2, \
                                             ' '.join((w for w in example)), str_format)

                    row += 1
            row += 2
            
        first_row = 1
        first_col = 3
        last_row = row
        last_col = first_col
        worksheet.data_validation(first_row, first_col, last_row, last_col,\
                                {'validate':'list', 'source':['True', 'False']})
    return