import pickle
from shutil import copy
from random import sample
from utils import get_windows
from operator import itemgetter
from xlsxwriter import Workbook
from collections import defaultdict, Counter

"""
This file is in charge of the exports.
    All exports are to .xlsx files.
"""



#######################################################################################
# Particular extra exports:
from profanity.profanity_export import export_profanities as profanity_extra_exports
# from emojicon.emojicon_export import export_emojicons as emojicon_extra_exports
# from punctuation.punctuation_export import export_puncts as punct_extra_exports
# from ALL_CAPS_SHOUTING.all_caps_export import export_all_caps as all_caps_extra_exports
#######################################################################################

def inofficialities_exports(input_dir, output_dirs,
                            detected_inos_pkl_paths, docs_pkl_path,
                            analyses_results_pkl_path):
    """
    In this method every task name that is passed from app.py should
    have a detected innoficial defaultdict that matches the general exports
    function demands. That means that the values are lists of dictionaries
    that consist the fields that are being request in those export functions.
    More over, (optional) this file navigate these `detected inos` to
    particular extra export functions for the analysis, if it is
    implemented and imported.  
    """
    with open(docs_pkl_path, 'rb') as docs_pkl_file:
        documents = pickle.load(docs_pkl_file)
    with open(analyses_results_pkl_path, 'rb') as analyses_results_pkl_file:
        analyses_results = pickle.load(analyses_results_pkl_file)

    individual_export_funcs = {
                   'Profanity':profanity_extra_exports,
                   # 'ALL_CAPS':all_caps_extra_exports,
                   # 'Emoji':emojicon_extra_exports,
                   # 'Punct':punct_extra_exports,
    }
    for task_name, path in detected_inos_pkl_paths.items():
        with open(path, 'rb') as det_inos_pkl_file:
            detected_inos_of_task = pickle.load(det_inos_pkl_file)

            # General exports:
            general_exports(task_name, input_dir,
                            output_dirs[task_name],
                            detected_inos_of_task, documents,
                            analyses_results[task_name],
            )

            # Particular exports:
            if task_name in individual_export_funcs:
                individual_export_funcs[task_name](input_dir, 
                            output_dirs[task_name],
                            detected_inos_of_task, documents,
                            analyses_results[task_name],
                )
    return 

def general_exports(task_name, input_dir,
                    output_dir, inos_found, documents,
                    analysis_results, maximal_col_width=80, fav_only=True):
    """
    This method is commanding the 3 .xlsx makers,
    giving each of them the info it needs to produce
    the exports it is in charge of.
    """

    print(f"writing general export files for '{task_name}'...")

    freq_dict = analysis_results['frequencies']
    most_ranked_dir = output_dir + "/most ranked bodies"
    excel_output_dir = output_dir + "/excel output files"

    write_ranked_bodies(task_name, input_dir, excel_output_dir,
                        freq_dict['freq_per_doc'], most_ranked_dir, maximal_col_width,
    )

    sents_by_ino = write_general(task_name, excel_output_dir,
                                 documents, inos_found, maximal_col_width,
    )

    write_ranked_inos(task_name, excel_output_dir, sents_by_ino,
        freq_dict['ino_counter'], freq_dict['dedup_ino_counter'], maximal_col_width,
    )

    return


def write_ranked_bodies(task_name, input_dir, excel_outdir, freq_per_doc,
                        docs_outdir, maximal_width, min_inos_in_dirty_doc=8):
    """
    produces a ranked (by amount of inos) email bodies .xlsx file in a descending order
    from the most inofficial body text to the least.
    """
    print("writing ranked bodies...")
    with Workbook(excel_outdir + '/most_ranked_email_bodies.xlsx') as workbook:
        worksheet = workbook.add_worksheet()
        str_format = workbook.add_format({'align': 'left'})
        center_format = workbook.add_format({'align': 'center'})
        header_format = workbook.add_format({'font_size': 11, 'align': 'center',
                                             'bg_color': '#C9FFE5', 'bold': 1,
                                             'border': 1,  
        })
        # get and set the columns' widths.
        headers = ['Doc ID', 'Occurrences']
        for i, width in enumerate(get_max_len_of_key_and_val(freq_per_doc, maximal_width)):
            worksheet.set_column(i, i, max(len(headers[i]), width))
        row = 1
        
        # setting the headlines:
        worksheet.write_string(0, 0, headers[0], header_format)
        worksheet.write_string(0, 1, headers[1], header_format)

        # writing the details:
        for fname, inos_count in \
                sorted(freq_per_doc.items(), key=itemgetter(1), reverse=True):
            worksheet.write_string(row, 0, fname, str_format)
            worksheet.write_number(row, 1, inos_count, center_format)
            row += 1

            # saves the files thats had a lot of inofficials in them:
            if inos_count < min_inos_in_dirty_doc:
                continue
            copy(input_dir + '/' + fname,
                f"{docs_outdir}/{task_name}_count={inos_count}_fname={fname}",
            )
    return

def get_max_len_of_key_and_val(data, maximum):
    max_len_k = max(max([len(key) for key in data]), maximum)
    max_len_v = max(max([v for v in data.values()]), maximum)
    return [max_len_k, len(str(max_len_v))]

def get_col_widths_from_defaultdict(data, headers, maximum):
    """
    This method finds the correct width for every column.
    In other words, it is the columns' widths auto-fit.
    """
    max_widths = defaultdict(int)
    max_widths[headers[0]] = max([len(k) for k in data])
    for key, vals_lst in data.items():
        for v in vals_lst: # v is a dictionary of an appearance
            for v_k, v_v in v.items():
                width = len(str(v_v))
                if width > max_widths[v_k]:
                    max_widths[v_k] = width
    return [min(maximum, width) for width in max_widths.values()]

def write_general(task_name, output_dir,
                  docs, found_inos, max_width):
    """
    This method writes all the found inofficial terms appearances that were found in the corpus. 
    """
    xlsx_model = "general"
    print("writing general...")
    with Workbook(output_dir + '/atomic_output.xlsx') as workbook:
        organized_exports, header2format, workbook = \
            organize_exports(task_name, xlsx_model,
                             workbook, found_inos, docs,)
        return xlsx_maker(workbook, xlsx_model,
                          header2format, organized_exports, max_width,)
            
def write_ranked_inos(task_name, output_dir, sents_by_ino, ino_counter,
                      dedup_ino_counter, maximal_width, examples_amount=5):
    """
    This method writes the found inofficial terms from the most popular decreasingly. 
    For each it present a max of `examples_amount` random sentences it has appeared in.
    """
    xlsx_model = "ranked"
    print("writing ranked inos...")
    with Workbook(output_dir + '/ranked_inofficials.xlsx') as workbook:
        rand_selected_occs = \
            rand_selected_examples(sents_by_ino, examples_amount)
        organized_exports, header2format, workbook = \
            organize_exports(task_name, xlsx_model, workbook,
                    [ino_counter, dedup_ino_counter, rand_selected_occs],)
        xlsx_maker(workbook, xlsx_model,
                   header2format, organized_exports, maximal_width,)
    return


def xlsx_maker(workbook, xlsx_model, header2format, organized_exports, max_width):
    """
    This method is getting all it needs to write the desired .xlsx file.
    The only thing left to do is measure the correct columns width, set them,
    set the headers and start writing the details row by row.

    Note:
    The general export is also collecting sentences that are returned
    and later getting randomly picked as example sentences for each found
    inofficial term.  
    """
    worksheet = workbook.add_worksheet()
    headers = [k for k in header2format]
    if xlsx_model == "general":
        sents_by_ino = defaultdict(list)
    blank_format = workbook.add_format({})
    header_format = workbook.add_format({'font_size': 11,
                                         'align': 'center',
                                         'bg_color': '#C9FFE5',
                                         'bold': 1, 'border': 1,  
    })
    col, row = 0, 1

    # Setting the columns widths:
    actual_widths = \
        get_col_widths_from_defaultdict(organized_exports, headers, max_width)
    for i, (width, header) in enumerate(zip(actual_widths, header2format)):
        worksheet.set_column(i, i, max(width, len(header)))

    # Setting the headers:
    for i, (header, _) in enumerate(header2format.items()):
        worksheet.write_string(col, i, header, header_format)
    
    # Writing the details:
    for ino, appearances in organized_exports.items():
        for apr in appearances:
            for i, (i_header, i_format) in enumerate(header2format.items()):

                info = apr[i_header]
                info_format = i_format if info else blank_format
                worksheet.write_string(row, col + i, info, info_format)

            # collecting the sentences, that soon would be randomly
            # picked as examples sentences for the current inofficial term.
            if xlsx_model == "general":
                sents_by_ino[ino].append(
                            (apr["Doc ID"], apr["Left Window"],
                             apr["Quote"], apr["Right Window"],
                            )
                )
            row += 1

    if xlsx_model == "general":
        return sents_by_ino
    return

def rand_selected_examples(data_dict, amount):
    '''
    This method gets a dictionary with ino terms as keys and a list
    of the sentences they appeared in throughout the corpus,
    and returns the same dictionary after a random selection of k 
    sentences (max) for each term.
    '''
    for key in data_dict:
        if len(data_dict[key]) > amount:
          data_dict[key] = sample(data_dict[key], k=amount)
    return data_dict

def organize_exports(task_name, xlsx_model, workbook, data, docs=None):
    """
    This method is setting the correct format and info for the excel files.
    Every export has its own order and formats.
    
    This method returns:
    --------------------
    organized : defaultdict(list)
        The well organize rows - ready to be written in the .xlsx file.
    header2format : dict
        A dictionary that map the headers to their formats.
    workbook : a xlsxwriter's Workbook
        The given workbook after being updated with the new formats it will need.

    """
    header2format = dict()
    organized = defaultdict(list)
    str_format = workbook.add_format({'align': 'left'})
    center_format = workbook.add_format({'align': 'center'})
    bold = workbook.add_format({'bold': 1, 'align': 'center'})
    rw_format = workbook.add_format({'align': 'left', 'italic': 1})
    lw_format = workbook.add_format({'align': 'right', 'italic': 1})
    ex_h_format = workbook.add_format({'align': 'left', 'bg_color':'#ffffe6'})
    term_format = workbook.add_format({'align': 'left', 'bg_color':'#ffd9b3', 'bold': 1, 'border': 1})
    term_info_format = workbook.add_format({'align': 'center', 'bg_color':'#ffd9b3', 'bold': 1, 'border': 1})
    
    ########################################################################################################
    if xlsx_model == "general":
    # output_dir + '/atomic_output.xlsx'
    ########################################################################################################
        for ino_term, ino_appears in data.items():
            for apr in ino_appears:
                wb_formats = dict()
                apr_by_task = dict()
                ######################################################

                apr_by_task["Term"] = str(ino_term)
                wb_formats["Term"] = term_format

                apr_by_task["Doc ID"] = str(apr["f_name"])
                wb_formats["Doc ID"] = str_format

                apr_by_task["Position\n(#line,start,end)"] = \
                    str((apr["line_num"],apr["start"],apr["end"]))
                wb_formats["Position\n(#line,start,end)"] = center_format

                apr_by_task["Score"] = str(apr["score"])
                wb_formats["Score"] = center_format

                apr_by_task["Label"] = str(apr["label"])
                wb_formats["Label"] = center_format

                ######################################################
                if task_name == "Profanity":

                    apr_by_task["support words"] = str(apr["support_words"])
                    wb_formats["support words"] = center_format
                ######################################################
                elif task_name == "Punct":
                    apr_by_task["stands alone"] = str(apr["stands_alone"])
                    wb_formats["stands alone"] = center_format

                    apr_by_task["cleaned word"] = str(apr["cleaned"])
                    wb_formats["cleaned word"] = center_format
                ######################################################
                elif task_name == "ALL_CAPS":
                    pass
                ######################################################
                elif task_name == "Emoji":

                    apr_by_task["info"] = str(apr["info"])
                    wb_formats["info"] = str_format

                    apr_by_task["cleaned word"] = str(apr["cleaned"])
                    wb_formats["cleaned word"] = center_format
                ######################################################

                apr_by_task["Left Window"] = str(apr["left_w"])
                wb_formats["Left Window"] = lw_format

                if task_name == "Profanity":
                    apr_by_task["Quote"] = str(apr["quote"])
                else:
                    apr_by_task["Quote"] = str(apr["original_token"])
                wb_formats["Quote"] = bold

                apr_by_task["Right Window"] = str(apr["right_w"])
                wb_formats["Right Window"] = rw_format

                apr_by_task["sentence"] = docs[apr["f_idx"]][apr["sent_idx"]]
                # No need to save a format, because we do not print it here.
                ######################################################
                if not header2format:
                    header2format = wb_formats
                organized[ino_term].append(apr_by_task) 
                ######################################################

    ########################################################################################################
    if xlsx_model == "ranked":
    
    # In this file there are two kind of rows:
    # 1) "term_row" -    Consist only the term and its number of
    #                    appearances (with and without duplicates)
    # 2) "example_row" - Consist an example and its file's name.
    ########################################################################################################
        ino_counter, dedup_ino_counter, rand_selected_occs = data
        for ino_term, appears_amount in sorted(ino_counter.items(), key=itemgetter(1), reverse=True):
            term_row = dict()
            wb_formats = dict()
            ######################################################
            # term row:
            term_row["Term"] = str(ino_term)
            wb_formats["Term"] = term_format

            term_row["Occurrences"] = str(appears_amount)
            wb_formats["Occurrences"] = term_info_format

            term_row["W/o duplicates"] = str(dedup_ino_counter[ino_term])
            wb_formats["W/o duplicates"] = term_info_format

            term_row["Example:"] = ''
            wb_formats["Example:"] = ex_h_format

            term_row["Left Window"] = ''
            wb_formats["Left Window"] = lw_format

            term_row["Quote"] = ''
            wb_formats["Quote"] = bold

            term_row["Right Window"] = ''
            wb_formats["Right Window"] = rw_format

            ######################################################
            if not header2format:
                header2format = wb_formats
            organized[ino_term].append(term_row)
            ######################################################
            # Example rows:
            for i, (fname, lw, quote, rw) in \
                    enumerate(rand_selected_occs[ino_term], start=1):

                # example row:
                example_row = dict()
                example_row["Term"] = ''
                example_row["Occurrences"] =''
                example_row["W/o duplicates"] = ''

                if len(rand_selected_occs[ino_term]) > 1:
                    example_row["Example:"] = f"{i}) from {fname} :"
                else:
                    example_row["Example:"] = f"from {fname} :"

                example_row["Left Window"] = lw
                example_row["Quote"] = quote
                example_row["Right Window"] = rw

                ######################################################
                organized[ino_term].append(example_row)
                ######################################################

    return organized, header2format, workbook