import re
import pickle
from collections import defaultdict

"""
"ALL-CAPS-SHOUTING" - detection upper-case shouting.
    As a part of the "inofficial" project, this task have to take its role
    before the words_index is going lower-case, of course, but after the
    punctuation cleaning.
"""


#################################################################################################################
"""
    Not all ALL-CAPS should consider as SHOUTINGs.
    These are examples for general type of unSHOUTING upper-case tokens:
"""
ignore_uppers = ['I',
                 'N', 'E', 'S', 'W', # Cardinal direction - consider ignore all SINGLE-CAPS.
                 'CC', 'RE', 'FW',
                 'TV', 'PC', 'AC',
                 'US', 'ISR', 'DE', 'EU' # country and areas short codes/names
                 'OK', # consider ignore all DOUBLE-CAPS.
                 'EDT', 'MIT', 'TNT', # companies, universities and brands..
                 'CPU', 'GPU', 'ASCII',
                 'BTW', 'FYI', 'SAP', 'ASAP', # terms. NOTE THAT THEY COULD BE ACTUALLY SHOUTING AS WELL!!
                 'HIV', # other diseases and medicines
                 'IPA', # consider ignore all TRPILE-CAPS.
                 # other conventions, such as constants in programming languages..
]

# NOTE:
# This list should be extended, or maybe it is better to ignore all ALL-CAPS with less than 4 letters,
# and have a smaller ignore list of tokens with more than 4 letters.
# Anyway, for now, in the lookup there is a line (stated as `an attempt`) that do just that.
#################################################################################################################


def totally_all_caps(token):
    return token.isupper()

def is_all_caps_candidate(token):
    return is_all_caps(token) and token not in ignore_uppers

def is_all_caps(token):
    return totally_all_caps(token) or not re.search('[a-zA-Z]', token)



def lookup(w_i_pkl_path):
    detected_AC_path = "ALL_CAPS_SHOUTING/detected_AC.pkl"
    with open(w_i_pkl_path, 'rb') as w_i_pkl_file:
        index = pickle.load(w_i_pkl_file)

    label = ''
    score = 100
    cleaned_word = ''
    index_size = len(index)
    detected = defaultdict(list)
    print('\n##################\n\nChecking the lexicon for ALL_CAPS...')

    # The lookup:
    for i, (word, appearances) in enumerate(index.items(), start=1):
        print(i, " out of ", index_size, " clean words ------ (ALL-CAPS-SHOUTING)")
        if is_all_caps_candidate(word):

            if len(word) < 4: # (this is an attempt)
                continue

            for appear in appearances:
                appear["label"] = label
                appear["score"] = score
                detected[word].append(appear)

    with open(detected_AC_path, 'wb') as detected_AC_pkl_file:
        pickle.dump(detected, detected_AC_pkl_file)
    return detected_AC_path
    #########################################################################################
    # The detected ALL-CAPS-SHOUTINGs are documented just like the words_index's appearances,
    # with extra particular details - and eventually looks like that: (fields names) 
    # {f_name ,f_idx ,sent_idx, idx_in_sent, start, end ,line_num,
    #  original_token, clean_of_punct, score, label}
    #########################################################################################
