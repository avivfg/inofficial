import time
import emoji
import codecs
import pickle
from os import path
from collections import defaultdict
# -*- coding: UTF-8 -*-

"""
"Emoji" - detection of Emojicons (Emojis and Emoticons).
    As a part of the "inofficial" project, this task have to take its role
    before the words_index is going through any change!
    If the words_index is being cleaned from punctuation or being lower-cased,
    we will loose information about emojicons!
"""


def load_emojicons():
    return load_emojis(), load_emoticons()

# This dictionary helps us reverse a given emoticon, so we can detect it
# whether if it was written left-to-right or right-to-left.
# Examples:   :)    will be reversed to   (:
#            :-b    will be reversed to   d-:
rev_dict = {'(':')', ')':'(', '[':']', ']':'[',
            '{':'}', '}':'{', '<':'>', '>':'<',
            ':':':', '|':'|', '#':'#', '$':'$',
            '%':'%', '^':'^', '*':'*', '-':'-',
            '_':'_', '+':'+', '=':'=', '!':'!',
            'A':'A', 'b':'d', 'd':'b', 'H':'H',
            'l':'l', 'm':'m', 'M':'M', 'o':'o', 
            'O':'O', 's':'s', 'S':'S', 'v':'v', 
            'V':'V', 'w':'w', 'W':'W', 'x':'x',
            'X':'X', '0':'0', '1':'1', '8':'8',
            '"':'"', "'":"'", '^':'^', ';':';',
            ',':',', '.':'.', '/':'/', '\\':'\\',
}

######################################################
########              GET EMOJIS:            #########
######################################################


def load_emojis():
    # consider using:   https://www.kaggle.com/eliasdabbas/how-to-create-a-regex-to-extract-emoji
    return {**emoji.UNICODE_EMOJI, **emoji.UNICODE_EMOJI_ALIAS}

def get_emoji_by_name(name, emoji_map):
    for em, em_name in emoji_map.items():
         if em_name == name:
            return em
    return None


########################################################
########             GET EMOTICONS:            #########
########################################################


def load_emoticons(fname='emoticons.txt', fdir='emojicon'):
    """
    This method loads the emoticons from the given file to a dictionary.
    It is also checks if it can be reversed, and add it to the dictionary, if so.
    """
    em_i = 0 # counter. this will help giving the emoticons a serial number for a name.
    emoti_dict = {}
    with open(fdir + '/' + fname, 'r') as rf:
        emoticons_lst = [emoticon for emoticon in rf.read().split('\n')]
        for em in emoticons_lst:
            if em in emoti_dict:
                continue
            em_i += 1
            # add a new emoticon:
            description = 'emoti-' + str(em_i)
            emoti_dict[em] = description
            # check the reversed emoticon:
            rev = reverse_emoticon(em)
            if rev and rev not in emoti_dict:
                # saving a reversed emoticon will be with the same name of its original
                # orientation emoticon's serial number, with an addition of a '-rev' string.
                emoti_dict[rev] = description + '-rev'
                # Example:
                #    if   :)  is called `emoti-666`, so the reversed,   (:  , will be called `emoti-666-rev`

    return emoti_dict

def reverse_emoticon(em):
    """
    This method uses the rev_dict to reverse emoticons, if possible.
    """
    rev_em = ''
    for c in reversed(em):
        if c in rev_dict:
            rev_em += rev_dict[c]
        else:
            return None
    if rev_em == em:
        return None
    return rev_em

########################################################
#######             LookUp functions:            #######
########################################################


def lookup(w_i_pkl_path):
    print('\n##################\n\nChecking the lexicon for emojis...')
    with open(w_i_pkl_path, 'rb') as w_i_pkl_file:
        index = pickle.load(w_i_pkl_file)

    index_size = len(index)
    detected = defaultdict(list)
    emoji_dict, emoti_dict = load_emojicons()
    for i, (token, appearances) in enumerate(index.items(), start=1):
        if len(token) > 50:
            continue
        print(i, " out of ", index_size, " raw tokens ------ (Emojis & Emoticons)")

        is_emojicon, emoj_key, emot_key, cleaned_word, score, label, info = \
                                emojiconizer(token, emoji_dict, emoti_dict) 
        print("after emojiconizer")
        if is_emojicon and score >= 50:
            key = emoj_key or emot_key
            print("after key")
            for appear in appearances:
                appear["info"] = info
                appear["label"] = label
                appear["score"] = score
                appear["cleaned"] = cleaned_word
                detected[key].append(appear)

    ##################################################################################
    # The detected emojicons are documented as the words_index's appearances,
    # but with extra particular details - and eventually looks like that: (fields names) 
    # {f_name ,f_idx ,sent_idx, idx_in_sent, start, end ,line_num,
    #  original_token, cleaned, score, label, info}
    ##################################################################################

    detected_emo_path = 'emojicon/detected_emos.pkl'
    with open(detected_emo_path, 'wb') as detected_emo_file:
        pickle.dump(detected, detected_emo_file, protocol=2)
    return detected_emo_path



def emojiconizer(tk, emoji_dict, emoti_dict):
    """
    This method is the main tool of the emojicons lookup.
    It finds out if the given token is an emoticon, an explicit/implicit emoji or none.
    When it is found, the method returns all the details about the founding.

    Note:
    as mentioned, an Emoji can be explicit or implicit (such as `:smile:`).

    """

    score = 0
    label = ''
    info = None 
    cleaned_tk = ''
    is_emojicon = False
    emoj, emot = None, None
    emoji_form = get_emoji_form(tk, emoji_dict)
    if emoji_form:
        score = 100
        is_emojicon = True
        emoj = tk if emoji_form == "explicit" else get_emoji_by_name(tk, emoji_dict)
        info = "Emoji: " + emoji_form + " form of " \
                         + emoj + " - " + emoji_dict[emoj]
        return is_emojicon, emoj, emot, cleaned_tk, score, label, info
    emot = find_emoti(tk, emoti_dict)
    if emot:
        is_emojicon = True

        # an emoticon was found.
        # now it is very important to report if it stands alone or being a part of a token.
        # This can actually change everything for good or worst.
        stands_alone = emot == tk
        
        cleaned_tk = tk.replace(emot,'')
        score = emoti_scorer(emot, tk, cleaned_tk, stands_alone)
        info = "Emoticon: " + emot + "  (" + emoti_dict[emot] + ")"
        if stands_alone:
            info += " (stands alone)"
        if len(cleaned_tk) + len(emot) < len(tk): # multiple occs
            return False, None, None, '', 0, '', None
    # more than 1 emoticon in a single token can point of a misinterpretation of the punctuation.
    if emot and find_emoti(cleaned_tk, emoti_dict):
        emot = None
        cleaned_tk = tk
        is_emojicon = False
        info = "Emoticon Error: multiple emoticons"
    
    return is_emojicon, emoj, emot, cleaned_tk, score, label, info


def emoti_scorer(emot, tk, cleaned_tk, stands_alone):
    """
    This method gives a score to the emoticon that was found in the token (tk),
    according to its position in the token.
    Of course, if the emoticon is the complete token itself, it is a 100%,
    and when it gets far from this situation, it is less and less obvious. 
    """
    score = 100
    if not stands_alone:
        if cleaned_tk:
            score -= (score/len(cleaned_tk))
        else: # it means that the token is a concatenation of the same emoticon.
            pass # it also means that cleaned_tk == 0, and we wouldn't want to divide by 0.
    if len(emot) == 2:
        score /= 2
    if any(c.isalnum() for c in emot):
        score /= 2
    return score

def find_emoti(tk, emoti_map):
    """
    This method finds emoticons within tokens, and return them if so.
    """
    if not tk:
        return False
    if tk in emoti_map:
        return tk
    for emoti in emoti_map:
        if emoti in tk:
            return emoti
    return None

def get_emoji_form(tk, emoji_map):
    """
    This function returns whether to given emoji is explicit or implicit.
    """
    if tk in emoji_map:
        return "explicit"
    if tk in emoji_map.values():
        return "implicit"
    return None