import pickle
from collections import defaultdict, Counter
# from emoji.analysis import analyze as emoji_analysis
# from punctuation.analysis import analyze as punctuation_analysis
# from ALL_CAPS_SHOUTING.analysis import analyze as all_caps_analysis
from profanity.analysis import get_collocations_dict as get_profanity_colls



'''
This file gets the lookups results and for each one:
    1) Calculates their frequencies (statistics).
    2) If implemented, sends them to their other analysis tests.
All the analyses results will be retrieved back to the app.py file,
and then will pass on to the exports phase.
'''



# Note:
# -----
# The only analysis that is implemented so far is the collocations analysis
# of the 'Profanity' detection task. All the rest still have only the frequencies
# calculation. BUT,
# It is also possible to get them through the Profanity's collocations analysis,
# if one thinks that it will give a meaningful result. 

def inofficialities_analyses(detected_inos_pkl_paths, docs_pkl_path):
    inos_analyses_results = defaultdict(dict)
    with open(docs_pkl_path, 'rb') as docs_pkl_file:
        docs = pickle.load(docs_pkl_file)

    collocation_funcs = {
                         # 'Emoji':profanity_analysis,
                         # 'Punct':profanity_analysis,
                         # 'ALL_CAPS': profanity_analysis,
                         'Profanity':get_profanity_colls,
    }

    # get frequencies:
    for task_name, path in detected_inos_pkl_paths.items():
        with open(path, 'rb') as det_inos_pkl_file:
            detected_inos_of_task = pickle.load(det_inos_pkl_file)
            inos_analyses_results[task_name] = \
                {"frequencies": get_freq_dict(detected_inos_of_task, docs,)}

            # get collocations (if implemented):
            if task_name in collocation_funcs:
                inos_analyses_results[task_name]["collocations"] = \
                    collocation_funcs[task_name](detected_inos_of_task, docs,
                inos_analyses_results[task_name]["frequencies"]["dedup_ino_counter"],
                )

    # pickle
    analyses_results_path = "analyses_results.pkl"
    with open(analyses_results_path, 'wb') as analyses_pkl_file:
        pickle.dump(inos_analyses_results, analyses_pkl_file)
    return analyses_results_path



def get_freq_dict(bad_words_found, documents):
    '''
    This method calculates:
    1) ino_counter       - The number of appearances of each found inofficial term.
    2) dedup_ino_counter - The number of appearances of each found inofficial term, without duplications.
    3) freq_per_doc      - The number of inofficial terms in each document.

    This method returns its results as a dictionary.
    '''
    freq_dict = dict()
    ino_counter = Counter()
    freq_per_doc = Counter()
    dedup_ino_counter = Counter()
    for ino_term, appearances in bad_words_found.items():
        non_dup_appears = list()
        ino_counter[ino_term] = len(appearances)
        for apr in appearances:
            sentence = documents[apr["f_idx"]][apr["sent_idx"]]
            freq_per_doc[apr["f_name"]] += 1
            non_dup_appears.append(sentence)
        # deduplicating:    
        dedup_ino_counter[ino_term] = len(set(non_dup_appears))

    freq_dict['dedup_ino_counter'] = dedup_ino_counter
    freq_dict['freq_per_doc'] = freq_per_doc
    freq_dict['ino_counter'] = ino_counter
    return freq_dict